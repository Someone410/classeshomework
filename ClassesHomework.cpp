#include <iostream>

class Homework
{
 private:
    int a;
  
 public:
    Homework(int NewA) : a(NewA)
    {}
 
    int GetA()
    {
        return a;
    }
};

class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void ShowXYZ()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }

    void ShowModuleXYZ()
    {
        std::cout << '\n' << sqrt(x*x + y*y + z*z);
    }
};

int main()
{
    Homework Var1(6146), Var2(-146);
    std::cout << Var1.GetA() << ' ' << Var2.GetA() << std::endl;

    Vector v(-2, 4, -4);

    std::cout << '\n' << "Vector with coordinates ";
    v.ShowXYZ();

    std::cout << '\n' << '\n' << "Has a length of";
    v.ShowModuleXYZ();
}